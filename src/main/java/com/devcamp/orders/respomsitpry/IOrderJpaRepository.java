package com.devcamp.orders.respomsitpry;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.orders.model.COrder;

public interface IOrderJpaRepository extends JpaRepository<COrder, Long> {
    
}
