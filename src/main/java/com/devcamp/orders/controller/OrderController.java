package com.devcamp.orders.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.orders.model.COrder;
import com.devcamp.orders.respomsitpry.IOrderJpaRepository;

@CrossOrigin
@RestController

public class OrderController {


    @Autowired 
    IOrderJpaRepository pIOrderJpaRepository;

    @GetMapping("/orders")
    public ResponseEntity<List<COrder>> getOrdeList(){
        try {
            List<COrder> orders = new ArrayList<COrder>();
            pIOrderJpaRepository.findAll().forEach(orders::add);

            return new ResponseEntity<>(orders, HttpStatus.OK);

            
        } catch (Exception e) {
            System.out.println("e.getMessage()");
            System.out.println(e.getMessage());
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
